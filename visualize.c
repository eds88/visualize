#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "linkedlist.h"
#include "datapoint.h"
#include "listsort.h"

static bool using_strict = false;
static bool using_custom_barsize = false;
static bool using_custom_labelsize = false;
static bool using_custom_symbol = false;
static bool using_verbose = false;
static long long bar_size = 20;
static long long label_size = 15;
static char *symbol = "o";

void print_usage(char *progname) {
    printf("\nusage: %s [OPTIONS] label1 value1 [label2 value2 [...]]\n                 "
        "[-s]/[--strict]\n                 "
        "[-barsize=size] [-labelsize=size]\n                 "
        "[-symbol=string]\n                 "
        "[-h]/[--help]\n                 "
        "[-v]/[--verbose]\n\n", progname);
}

long long strtoll_errorcheck(char *str, int base, bool strict) {
    char *endptr;
    long long value_buffer = strtoll(str, &endptr, base);

    if (errno == ERANGE) {
        fprintf(stderr, "error: string \"%s\" is out of range.\n", str);
        exit(EXIT_FAILURE);
    }
    else if (endptr == str) {
        fprintf(stderr, "error: string \"%s\" is not an integer.\n", str);
        exit(EXIT_FAILURE);
    }
    else if (*endptr != '\0') {
        if (strict) {
            fprintf(stderr, "error: non-numerical character detected in string \"%s\"\n", str);
            exit(EXIT_FAILURE);
        }
        else
            fprintf(stderr, "warning: non-numerical character detected in string \"%s\"\n", str);
    }
    
    return value_buffer;
}

char *substr_after_index(char *str, int index) {
    char *substr = malloc((strlen(str) - index + 1) * sizeof(char*));

    // last char of substr is guarunteed to be '\0', assuming str is a proper string
    unsigned int k = 0;
    for (unsigned int i=index; i<strlen(str); i++) {
        substr[k] = str[i];
        k++;
    }
    return substr;
}

void print_settings() {
    puts("");

    if (using_strict)
        puts("using strict mode (exit on bad parse)");
    else
        puts("using non-strict mode (accept bad parse if number is found)");
    
    if (using_custom_barsize)
        printf("using custom ");
    printf("bar size: %lld\n", bar_size);

    if (using_custom_labelsize)
        printf("using custom ");
    printf("label size: %lld\n", label_size);

    if (using_custom_symbol)
        printf("using custom ");
    printf("symbol: %s\n", symbol);
}

int parse_optional_args(int argc, char** argv) {
    int num_options = 0; // argv[0] == progname

    for (int i=1; i<argc; i++) {
        if (0 == strcmp(argv[i], "--strict") || 0 == strcmp(argv[i], "-s")) {
            using_strict = true;
            num_options++;
        }
        else if (0 == strncmp(argv[i], "-barsize=", 9)) {
            using_custom_barsize = true;
            char *substr = substr_after_index(argv[i], 9);
            bar_size = strtoll_errorcheck(substr, 10, true);
            num_options++;
        }
        else if (0 == strncmp(argv[i], "-labelsize=", 11)) {
            using_custom_labelsize = true;
            char *substr = substr_after_index(argv[i], 11); 
            label_size = strtoll_errorcheck(substr, 10, true);
            num_options++;
        }
        else if (0 == strncmp(argv[i], "-symbol=", 8)) {
            using_custom_symbol=true;
            char *substr = substr_after_index(argv[i], 8); 
            symbol = substr;
            num_options++;
        }
        else if (0 == strcmp(argv[i], "-h") || 0 == strcmp(argv[i], "--help")) {
            print_usage(argv[0]);
            exit(EXIT_SUCCESS);
            num_options++;
        }
        else if (0 == strcmp(argv[i], "-v") || 0 == strcmp(argv[i], "--verbose")) {
            using_verbose = true;
            num_options++;
        }
    }

    if (using_verbose)
        print_settings();

    return num_options;
}

/*
 * Print an error message and exit if bad args.
 */
void assert_arg_validity(int argc, char** argv, int num_options) {
    if (using_verbose && argc - num_options == 3) {
        fprintf(stderr, "\nwarning: only one data point (was this intended?)\n");
    }
    else if ((argc - num_options - 1) % 2) {
        fprintf(stderr, "error: dangling label\n");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }
    else if (argc - num_options < 3) {
        fprintf(stderr, "\nerror: no data");
        print_usage(argv[0]);
        exit(EXIT_FAILURE);
    }
}

struct linkedlist *parse_args(char** argv, int num_options, int size, struct data_point *data_points) {
    struct linkedlist *entries = linkedlist_init();

    // idea here is argv[j] == label and argv[j+1] == value 
    int j = num_options+1; 
    for (int i=0; i<size; i++) { 
        data_points[i].label = argv[j];
        char *value_string = argv[j+1];
        
        // catch out of range errors and print an error message
        long long value_buffer = strtoll_errorcheck(value_string, 10, using_strict);
        data_points[i].value = value_buffer;

        linkedlist_append(entries, &data_points[i]);
        j+=2;
    }
    
    return entries;
}

void print_bar_graph(struct linkedlist *data_points) {
    struct linkedlist *sorted = listsort(data_points->head, false, false);
    struct ll_node *current = sorted->head;
    long long max_value = ((struct data_point*)(sorted->tail->data))->value;

    puts("");
    while (current) {
        struct data_point *curr_data_point = (struct data_point*)current->data;
        double num_symbols_to_print = ((double) curr_data_point->value / (double) max_value ) * bar_size;
        printf("%-*.*s ", (int)label_size, (int)label_size, curr_data_point->label);

        // print the required number of symbols for this data point
        for (long long i=0; i<num_symbols_to_print; i++) {
            printf("%s", symbol);
        }
        printf("\n");
        
        current = current->next;
    } 
    puts("");
}

int main(int argc, char **argv) {
    // exit on bad args
    int num_options = parse_optional_args(argc, argv);
    assert_arg_validity(argc, argv, num_options);

    // allocate heap memory for the data points
    // (heap-allocated arrays are safer than VLAs when made large)
    int size = (argc - num_options - 1)/2;
    struct data_point *data = malloc(size * sizeof(struct data_point));

    // parse the arguments and store a linkedlist of (pointers to) data_point
    struct linkedlist *data_points = parse_args(argv, num_options, size, data);
    print_bar_graph(data_points);

    // free all allocated heap memory
    linkedlist_dealloc(data_points);
    free(data);

    return 0;
}

