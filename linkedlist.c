#include <stdio.h>
#include <stdlib.h>
#include "linkedlist.h"

struct linkedlist *linkedlist_init() {
    struct linkedlist *self = malloc(sizeof(struct linkedlist));
    self->head = NULL;
    self->tail = NULL;

    return self;
}

/*
 * Returns NULL if nothing found
 */
struct ll_node *linkedlist_get(struct linkedlist *self, void *data) {
    if (!self->head) return NULL;
    else {
        if (self->head->data == data)
            return self->head;
        else {
            if (!self->tail)
                return NULL;
            
            // "normal" case -- list is rather populated
            else { 
                struct ll_node *current = self->head->next;
                while (current) { 
                    if (current->data == data)
                        return current;
                    current = current->next;
                }
                return NULL;
            }
        }
    }
}

void linkedlist_append(struct linkedlist *self, void *data) {
    struct ll_node *current = malloc(sizeof(struct ll_node));
    current->data = data;
    current->next = NULL;

    if (!self->head)
        self->head = current;
    else
        self->tail->next = current;

    self->tail = current;
}

void linkedlist_prepend(struct linkedlist *self, void *data) {
    if (!self->head)
        linkedlist_append(self, data);
    else {
        struct ll_node *current = malloc(sizeof(struct ll_node));
        current->data = data;
        current->next = self->head;
        self->head = current;
    }
}

void linkedlist_insert_after(struct linkedlist *self, struct ll_node *node, void* data) {
    if (node == self->tail)
       linkedlist_append(self, data);
    else {
        struct ll_node *current = malloc(sizeof(struct ll_node));
        current->data = data;
        current->next = node->next;
        node->next = current;
    }
}

static struct ll_node *linkedlist_get_prev(struct linkedlist *self, struct ll_node *node) {
    struct ll_node* current = self->head;
    
    while(current) {
        if (current->next = node)
            return current;
        current = current->next;
    }

    return NULL;
}

void linkedlist_remove_node(struct linkedlist *self, struct ll_node *node) {
    struct ll_node *prev = linkedlist_get_prev(self, node);
    if (!prev)
        return;

    prev->next = node->next;
    free(node);
}

void linkedlist_remove_instances_of(struct linkedlist *self, void *data) {
    if (!self->head)
        return;

    struct ll_node *prev;
    struct ll_node *current = self->head;      
    while (current) {
        if (current == self->head) {
            if (self->head->data == data) {
                struct ll_node *this = self->head;
                self->head = self->head->next;
                current = self->head;
                free(this);
            }
            else {
                prev = self->head;
                current = self->head->next;
            }
        }
        else {
            if (current->data == data) {
                prev->next = current->next;
                struct ll_node *this = current;
                current = current->next;
                if (this == self->tail)
                    self->tail = prev;

                free(this);
            }
            else {
                struct ll_node *buffer = current;
                current = current->next;
                prev = buffer;
            }
        }
    }
}

void linkedlist_print(struct linkedlist *self) {
    int count = 0;
    if (self->head) {
        struct ll_node *current = self->head;

        while (current) {
            printf("Node %d points to %p\n", count, current->data);
            current = current->next;
            count++;
        }
        printf("\n");
    }
    else
        fprintf(stderr, "Error: empty linked list\n");
}

void linkedlist_dealloc(struct linkedlist *self) {
    struct ll_node *current = self->head;
    while (current) {
        struct ll_node *buffer = current->next;
        free(current);
        current = buffer;
    }
    free(self);
}
