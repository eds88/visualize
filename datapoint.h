#ifndef _DATAPOINT_H
#define _DATAPOINT_H

/* Listsort depends on struct data_point but I wanted it separate from the rest of the code.
 * So now it can just include this bit, while making sure it doesn't redefine the struct. */
struct data_point {
    char *label;
    long long value;
};

#endif
