#ifndef _LISTSORT_H
#define _LISTSORT_H

#include <stdbool.h>
#include "linkedlist.h"

struct linkedlist *listsort(struct ll_node *list, bool is_circular, bool is_double);

#endif
