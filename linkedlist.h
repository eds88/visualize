#ifndef _LINKEDLIST_H
#define _LINKEDLIST_H

struct ll_node {
    void *data;
    struct ll_node *next;
};

struct linkedlist {
    struct ll_node *head, *tail;
};

struct linkedlist *linkedlist_init();
struct ll_node *linkedlist_get(struct linkedlist *self, void *data);
void linkedlist_append(struct linkedlist *self, void *data);
void linkedlist_prepend(struct linkedlist *self, void *data);
void linkedlist_insert_after(struct linkedlist *self, struct ll_node *node, void *data);
void linkedlist_remove_node(struct linkedlist *self, struct ll_node *data);
void linkedlist_remove_instances_of(struct linkedlist *self, void *data);
void linkedlist_print(struct linkedlist *self);
void linkedlist_dealloc(struct linkedlist *self);

#endif

