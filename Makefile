CC=gcc
CFLAGS=-O2 -std=iso9899:2011 -I.
DEPS=linkedlist.c listsort.c
SRC=$@.c

visualize:
	gcc -o $@ $^ $(CFLAGS) $(SRC) $(DEPS)
