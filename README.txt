=========
VISUALIZE
=========
Accepts a labeled list of integers, and prints a sorted (least to greatest) bar graph visually demonstrating
the relative sizes of the data.

(c)2016 Elisha Sword.

=============
EXAMPLE USAGE
=============
$ ./visualize apples 15 oranges 2 limes 3 lemons 1 pies 0

    pies            
    lemons          oo
    oranges         ooo
    limes           oooo
    apples          oooooooooooooooooooo

$ ./visualize -barsize=15 -labelsize=7 apples 15 oranges 2 limes 3 lemons 1 pies 0

    pies    
    lemons  o
    oranges oo
    limes   ooo
    apples  ooooooooooooooo
    
$ ./visualize -barsize=3 -labelsize=10 apples 15 oranges 2 limes 3 lemons 1 pies 0

    pies       
    lemons     o
    oranges    o
    limes      o
    apples     ooo
    
$ ./visualize -barsize=3 -labelsize=2 "-symbol=:^) " apples 15 oranges 2 limes 3 lemons 1 pies 0

    pi    
    le :^) 
    or :^)
    li :^) 
    ap :^) :^) :^) 
    
    
Run
    $ ./visualize -h 
for a full list of paramaters.

============
DEPENDENCIES
============
 - eds88 linkedlist (included)
 - Simon Tatham's listsort (included and modified)

=====
TODOS
=====
 - support negative numbers
 - enforce (warn about?) maximum bar/label size, symbol length (only in strict mode?)
 - create AUR package
